from xml.etree import ElementTree as ET
import sys

def view_file(filename):
    root = ET.parse(filename).getroot()
    output = [filename.split('/')[-1][:-4], '']
    speakers = {}
    for speaker in root.findall(".//Speaker"):
        speakers[speaker.get('id')] = speaker.get('name')
        output.append('Speaker: %s = %s' % (speaker.get('id'), speaker.get('name')))
    output.append('')
    for turn in root.findall(".//Turn"):
        speaker = 'unknown'
        if 'speaker' in turn.attrib:
            speaker = ';'.join(['%s' % x for x in turn.get('speaker').split()])
        text = []
        if turn.text != None:
            text.append(turn.text.strip())
        for node in turn:
            text.append(node.tail.strip())
        text = [x for x in text if x != '']
        if len(text) > 0:
            output.append(speaker + ': ' + ' '.join(text))
    import pydoc
    pydoc.pager('\n'.join(output))

if __name__ == '__main__':
    import readline
    directory = sys.argv[1]
    while True:
        filename = raw_input('tsv> ')
        try:
            view_file(directory + '/' + filename + '.trs')
        except IOError:
            print 'Error: could not load', filename
