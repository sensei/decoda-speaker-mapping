from xml.etree import ElementTree as ET
import sys

from collections import defaultdict
speakers = defaultdict(list)

for filename in sys.argv[1:]:
    root = ET.parse(filename).getroot()
    turns = root.findall(".//Turn")
    for speaker in root.findall(".//Speaker"):
        num_turns = 0
        speaker_id = speaker.get("id")
        for turn in turns:
            if 'speaker' in turn.attrib and speaker_id in turn.get("speaker").strip().split():
                num_turns += 1
        if num_turns > 0:
            speakers[speaker.get("name")].append(filename.split("/")[-1][:-4])

for speaker, files in sorted(speakers.items(), key=lambda x: x[0]):
    label = 'unknown'
    if speaker in ['Apellant', 'Appelant', 'Appelant 1', 'Appelant 2', 'ami', 'appelan', 'appelant', 'appelant ', 'appelant 2', 'appelant-fille', 'appelant1', 'appelant2', 'appelant_F02', 'appellant', 'collègue', 'demandeur'] or 'appelant' in speaker.lower():
        label = 'caller'
    if 'conseiller' in speaker.lower() or 'agent' in speaker.lower() or speaker in ['conseillet', 'conseilller', 'consiller', 'conseiler', 'conseilelr']:
        label = 'agent'
    print(';'.join([label, speaker, ",".join(files)]))

