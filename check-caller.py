import sys
from collections import defaultdict

speakers = defaultdict(list)

for line in sys.stdin:
    label, name, shows = line.strip().split(';')
    for show in shows.split(','):
        speakers[show].append(label)

for show, names in speakers.items():
    if 'caller' not in names:
        print show, names

