from xml.etree import ElementTree as ET
import sys, os

from collections import defaultdict
speakers = defaultdict(list)

mapping = defaultdict(lambda: defaultdict(str))

for line in open('mapping.txt'):
    speaker_type, text, shows = line.strip().split(';')
    for show in shows.split(','):
        mapping[show][text] = speaker_type

for filename in sys.argv[1:]:
    show = os.path.basename(filename).split('.')[0]
    root = ET.parse(filename).getroot()
    turns = root.findall(".//Turn")
    for speaker in root.findall(".//Speaker"):
        num_turns = 0
        speaker_id = speaker.get("id")
        speaker_type = mapping[show][speaker.get('name')]
        if speaker_type != '':
            print '\t'.join([show, speaker_id, speaker_type, speaker.get('name')])

